Name:           kaizen-default-settings
Version:        0.9
Release:        11%{?dist}
Summary:        The default settings for Kaizen
License:        GPLv3+
URL:            https://gitlab.com/kaizen-os/default-settings
Source0:        %{url}/-/archive/%{version}/default-settings-%{version}.tar.gz

BuildArch: noarch

%description
The default settings for Kaizen

%prep
%autosetup -n default-settings-%{version}

%install
#mkdir -p %{buildroot}/etc/io.elementary.appcenter/
#cp appcenter.blacklist %{buildroot}/etc/io.elementary.appcenter/

mkdir -p %{buildroot}/etc/skel/
cp .inputrc %{buildroot}/etc/skel/

mkdir -p %{buildroot}/etc/skel/.config/plank/dock1/launchers/
cp plank/dock1/launchers/* %{buildroot}/etc/skel/.config/plank/dock1/launchers/

mkdir %{buildroot}/etc/profile.d/
cp profile.d/*.sh %{buildroot}/etc/profile.d/

# replace with datadir probably
mkdir -p %{buildroot}%{_prefix}/share/applications/
cp sessioninstaller.desktop %{buildroot}%{_prefix}/share/applications/

mkdir -p %{buildroot}/etc/gtk-3.0/
cp settings.ini %{buildroot}/etc/gtk-3.0/

mkdir -p %{buildroot}/etc/skel/.config/gtk-3.0/
cp gtk-3.0/settings.ini %{buildroot}/etc/skel/.config/gtk-3.0/

mkdir -p %{buildroot}/etc/sudoers.d/
cp sudoers.d/* %{buildroot}/etc/sudoers.d/

mkdir -p %{buildroot}/%{_datadir}/glib-2.0/schemas
cp -p overrides/default-settings.gschema.override %{buildroot}/%{_datadir}/glib-2.0/schemas/

# These scriptlets are needed because .override files don't trigger the schema recompilation
%postun
if [ $1 -eq 0 ] ; then
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files
%defattr(-,root,root,-)
#/etc/io.elementary.appcenter/*
/etc/skel/.config/gtk-3.0/settings.ini
/etc/skel/.config/plank/dock1/*
/etc/profile.d/*.sh
/usr/share/applications/sessioninstaller.desktop
/etc/gtk-3.0/settings.ini
/etc/sudoers.d/*
/etc/skel/.inputrc
%{_datadir}/glib-2.0/schemas/default-settings.gschema.override

%changelog
* Tue Oct 23 2018 Cody Garver <codygarver@gmail.com> - 0.9-1
- Initial version of the package
